# Agent Installation
For Agent Installation create any windows machine in azure virtual machine, with required configuration.

## Create Personal Access Token
Create a Azure Devops Project and Goto your project page on Azure devOps -> Then click on Project Settings as shown

![Project settings](./.doc/1.png)

Then click on User settings.

![user settings](./.doc/2.png)

and select Personal access Token. Click on 'New Token' and give required access to token and create new Token. Note the token for future use.

## Setup Windows Agent Machine

Connect to windows machine which is to be configured as agent to run the pipeline jobs.
Click on Connect and choose RDP

![Rdp windows](./.doc/3.png)

Then Download the RDP file to connect the machine.
Open The RDP file on your local machine and Connect with username and password used while creating the VM.

In Windows VM open the Browser and Download the [Agent package](https://vstsagentpackage.azureedge.net/agent/2.204.0/vsts-agent-win-x64-2.204.0.zip) Then extract the package in Agent folder in C drive.

![enter image description here](./.doc/4.png)

Run the Powershell with admin privileges, Change the directory to the Agent folder.
Now Run the following command in powershell.

    .\config.cmd

Follow the steps to enter the details. Enter the PAT (Personal Access Token), Enter the login username and password, Select 'N' for 'Prevent Service starting Immediately...'

![enter image description here](./.doc/5.PNG)

Then Run,

    .\run.cmd
After running commands, A new agent will be available in Agent Pools in Project settings.
![enter image description here](./.doc/6.png)

# Install the Project prerequisites

## Install Openssl

To install prerequisites, Connect to Windows Agent Machine with RDP, Open the Powershell with admin access and run the following command to install Chocolatey

    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

After chocolatey installation, Close the powershell and start the powershell again with admin access.
Run the following command to install Openssl

    choco install openssl -y
The command will install openssl on you windows agent machine.

## Install Azure Powershell Module

To install azure powershell module go to this [GitHub](https://github.com/Azure/azure-powershell/releases) link to download the latest msi package for the windows. Install the package in Windows agent Machine
![msipackage](./.doc/7.png)

# Setup Azure Pipeline
Before setting the azure pipeline we need to create a service principle to give key vault access to agent machine.

## Create Service Principle

Login to [Azure Portal](https://portal.azure.com/) and open up Azure CLI then input the following command to create a service principle. Replace 'test-sp' with your service principle name.

    export SERVICE_PRINCIPAL_CLIENT_SECRET="$(az ad sp create-for-rbac --skip-assignment --name http://test-sp --query 'password' -otsv)"
 Then echo to save the service principle secret to use it later.
 

    echo $SERVICE_PRINCIPAL_CLIENT_SECRET
 Then get the service principle ID, Note these vaules for future use. 

    export SERVICE_PRINCIPAL_CLIENT_ID="$(az ad sp list --display-name http://test-sp --query 'appId' -otsv)"
    echo $SERVICE_PRINCIPAL_CLIENT_ID
To give service principle access to key vault login to [Azure Portal](https://portal.azure.com/) and goto your key vault.
![enter image description here](./.doc/8.png)

Select Access Policies,  Then Click on 'Add Access Policy'
On next page add permission for the secrets category and click on 'Select Principle'.
![enter image description here](./.doc/9.png)

Now in Search Bar Enter the Service Principle ID which we earlier created and then click on ADD.
Now Service Principle will be shown in APPLICATION category, Then click on 'Save' to save the changes.
![enter image description here](./.doc/10.png)

## Create Pipeline job
Goto [Azure Devops](https://dev.azure.com/) Select your project and Click on Pipelines, Select 'New Pipeline' and Select the Repository where your pipeline code is located.

![enter image description here](./.doc/11.png)

On Configure tab, Select Existing Azure pipeline Yaml file and give your pipeline yaml path. Then click on Continue to Create Pipeline.

After Creating pipeline your pipeline will be listed. Then click on that pipeline and select 'Edit'.

![enter image description here](./.doc/12.png)

Then Click on Variables Tab to set the variables required for pipeline.
Then add the pipeline variables one by one as shown in picture.

![enter image description here](./.doc/13.png)

You can get TenantID by clicking on 'JSON view' on key vault overview page in Azure Portal.

Then Click on Save. Now Your pipeline is set up.
